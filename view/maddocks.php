<!DOCTYPE html>
<html lang="en">
    <?php include('../include/header.php'); ?>
    <body>
        <?php include('../include/navigation.php'); ?>
        <div class="container">
            <h1>SCAT3</h1>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Maddock's Score</h3>
                </div>
                <div class="panel-body">
                    <form role="form">

                        <!-- VENUE -->
                        <strong>What venue are we at today?</strong>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="maddox-venue-checkbox"> Correct
                            </label>
                        </div>

                        <hr />

                        <!-- HALF -->
                        <strong>Which half is it now?</strong>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="maddox-half-checkbox"> Correct
                            </label>
                        </div>

                        <hr />

                        <!-- LAST SCORE -->
                        <strong>Who scored last in this match?</strong>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="maddox-last-score-checkbox"> Correct
                            </label>
                        </div>

                        <hr />

                        <!-- LAST TEAM -->
                        <strong>What team did you play last week / game?</strong>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="maddox-last-team-checkbox"> Correct
                            </label>
                        </div>

                        <hr />

                        <!-- LAST-VICTORY -->
                        <strong>Did your team win the last game?</strong>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="maddox-last-game-win-checkbox"> Correct
                            </label>
                        </div>

                        <hr />

                        <button type="submit" class="btn btn-default">Next</button>
                    </form>
                </div>
            </div>
        </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
    </body>
</html>
