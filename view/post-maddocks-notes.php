<!DOCTYPE html>
<html lang="en">
    <?php include('../include/header.php'); ?>
    <body>
        <?php include('../include/navigation.php'); ?>
        <div class="container">
            <h1>SCAT3</h1>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Notes</h3>
                </div>
                <div class="panel-body">
                    <form role="form">

                        <!-- VENUE -->
                        <strong>Mechanism of injury (“tell me what happened”?):</strong>
                        <textarea class="form-control" rows="3"></textarea>

                        <hr />

                        <button type="submit" class="btn btn-default">Next</button>
                    </form>
                </div>
            </div>
        </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
    </body>
</html>
