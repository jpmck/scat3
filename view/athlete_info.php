<!DOCTYPE html>
<html lang="en">
    <?php include('../include/header.php'); ?>
    <body>
        <?php include('../include/navigation.php'); ?>
        <div class="container">
            <h1>SCAT3</h1>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Initial Assessment</h3>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form">

                        <!-- ATHLETE'S NAME -->

                        <label>Athlete's Name</label>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="athlete-first-name">First Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="athlete-first-name" placeholder="First">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="athlete-last-name">Last Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="athlete-last-name" placeholder="Last">
                            </div>
                        </div>

                        <hr />

                        <!-- DATE AND TIME OF INJURY -->

                        <label>Date and Time of Injury</label>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="dateInjury">Date</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" id="dateInjury" placeholder="Date of Injury">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="timeInjury">Time</label>
                            <div class="col-sm-10">
                                <input type="time" class="form-control" id="timeInjury" placeholder="Time of Injury">
                            </div>
                        </div>

                        <hr />

                        <!-- DATE AND TIME OF ASSESSMENT -->

                        <label>Date and Time of Assessment</label>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="date-assessment">Date</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" id="date-assessment" placeholder="Date of Injury">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="time-assessment">Time</label>
                            <div class="col-sm-10">
                                <input type="time" class="form-control" id="time-assessment" placeholder="Time of Injury">
                            </div>
                        </div>

                        <button type="submit" class="btn btn-default">Next</button>
                    </form>


                </div>
            </div>
        </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
    </body>
</html>
