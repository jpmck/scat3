<!DOCTYPE html>
<html lang="en">
  <?php include('../include/header.php'); ?>
  <body>
      <?php include('../include/navigation.php'); ?>
      <div class="container">
          <h1>SCAT3</h1>
          <div class="panel panel-default">
              <div class="panel-heading">
                  <h3 class="panel-title">Initial Assessment</h3>
              </div>

              <div class="panel-body">
                  <form role="form">

                      <!-- ATHLETE'S NAME -->

                      <label>Potential Signs of Concussion?</label>

                      <em>If any of the following signs are observed after a direct or indirect blow to the head, the athlete should stop participation, be evaluated by a medical professional and should not be permitted to return to sport the same day if a concussion is suspected.</em>

                      <hr />

                      <!-- LOSS OF CONSCIOUSNESS? -->
                      <strong>Any loss of consciousness?</strong>
                      <div class="checkbox">
                          <label>
                              <input type="checkbox" id="loss-of-consciousness-checkbox"> Yes
                          </label>
                      </div>

                      <div class="form-group">
                          <label for="loss-of-consciousness-text">If so, how long?</label>
                          <input type="text" class="form-control" id="loss-of-consciousness-text" placeholder="How Long?">
                      </div>

                      <hr />

                      <!-- BALANCE ISSUES? -->
                      <strong>Balance or motor incoordination (stumbles, slow / laboured movements, etc.)?</strong>
                      <div class="checkbox">
                          <label>
                              <input type="checkbox" id="balance-or-motor-incoordination-checkbox"> Yes
                          </label>
                      </div>

                      <hr />

                      <!-- DISORIENTATION ISSUES? -->
                      <strong>Disorientation or confusion (inability to respond appropriately to questions)?</strong>
                      <div class="checkbox">
                          <label>
                              <input type="checkbox" id="disorientation-or-confusion-checkbox"> Yes
                          </label>
                      </div>

                      <hr />

                      <!-- LOSS OF MEMORY? -->
                      <strong>Any loss of memory?</strong>
                      <div class="checkbox">
                          <label>
                              <input type="checkbox" id="loss-of-memory-checkbox"> Yes
                          </label>
                      </div>

                      <div class="form-group">
                          <label for="loss-of-consciousness-text">If so, how long?</label>
                          <input type="text" class="form-control" id="loss-of-memory-text" placeholder="How Long?">
                      </div>

                      <strong>Before or after the injury?</strong>
                      <div class="radio">
                          <label>
                              <input type="radio" name="init-loss-of-memory-radio" id="init-loss-of-memory-radio-before" value="before">
                              Before
                          </label>
                      </div>
                      <div class="radio">
                          <label>
                              <input type="radio" name="init-loss-of-memory-radio" id="init-loss-of-memory-radio-after" value="after">
                              After
                          </label>
                      </div>

                      <hr />

                      <button type="submit" class="btn btn-default">Next</button>
                  </form>


              </div>
          </div>
      </div>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="../js/bootstrap.min.js"></script>
  </body>
</html>
