<!DOCTYPE html>
<html lang="en">
    <?php include('../include/header.php'); ?>
    <body>
        <?php include('../include/navigation.php'); ?>
        <div class="container">
            <h1>SCAT3</h1>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Glasgow Coma Scale (GCS)</h3>
                </div>
                <div class="panel-body">
                    <form role="form">
                        <!-- EYE RESPONSE (E) -->
                        <strong>Best Eye Response (E)</strong>
                        <div class="radio">
                            <label>
                                <input type="radio" name="GCS-E-Radio" id="GCS-E-Option1" value="GCS-E-1">
                                No eye opening
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="GCS-E-Radio" id="GCS-E-Option2" value="GCS-E-2">
                                Eye opening in response to pain
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="GCS-E-Radio" id="GCS-E-Option3" value="GCS-E-3">
                                Eye opening to speech
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="GCS-E-Radio" id="GCS-E-Option4" value="GCS-E-4">
                                Eyes opening spontaneously
                            </label>
                        </div>
                        <!-- END EYE RESPONSE (E) -->

                        <!-- VERBAL RESPONSE (V) -->
                        <strong>Best Verbal Response (V)</strong>
                        <div class="radio">
                            <label>
                                <input type="radio" name="GCS-V-Radio" id="GCS-V-Option1" value="GCS-V-1">
                                No verbal response
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="GCS-V-Radio" id="GCS-V-Option2" value="GCS-V-2">
                                Incomprehensible sounds
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="GCS-V-Radio" id="GCS-V-Option3" value="GCS-V-3">
                                Inappropriate words
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="GCS-V-Radio" id="GCS-V-Option4" value="GCS-V-4">
                                Confused
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="GCS-V-Radio" id="GCS-V-Option5" value="GCS-V-5">
                                Oriented
                            </label>
                        </div>
                        <!-- END VERBAL RESPONSE (V)-->

                        <!-- MOTOR RESPONSE (M) -->
                        <strong>Best Motor Response (M)</strong>
                        <div class="radio">
                            <label>
                                <input type="radio" name="GCS-M-Radio" id="GCS-M-Option1" value="GCS-M-1">
                                No motor response
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="GCS-M-Radio" id="GCS-M-Option2" value="GCS-M-2">
                                Extension to pain
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="GCS-M-Radio" id="GCS-M-Option3" value="GCS-M-3">
                                Abnormal flexion to pain
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="GCS-M-Radio" id="GCS-M-Option4" value="GCS-M-4">
                                Flexion / Withdrawal to pain
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="GCS-M-Radio" id="GCS-M-Option5" value="GCS-M-5">
                                Localizes to pain
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="GCS-M-Radio" id="GCS-M-Option6" value="GCS-M-6">
                                Obeys commands
                            </label>
                        </div>
                        <!-- END MOTOR RESPONSE (M) -->

                        <button type="submit" class="btn btn-default">Next</button>
                    </form>
                </div>
            </div>
        </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
    </body>
</html>
